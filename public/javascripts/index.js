/**
 * Created by benja on 12/01/2017.
 */

(function() {

    if (localStorage.getItem("registered") == "true") {
        $('.welcome-bar').show();
        $('.welcome-text').text("Welcome, " + localStorage.getItem("attendee_name"));
    } else {
        $('.welcome-bar').hide();
    }



})();