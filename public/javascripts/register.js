/**
 * Created by benja on 12/01/2017.
 */

(function() {

    if (localStorage.getItem("registered") == "true") {
        $('.register-form').hide();
        $('.thankyou').text("Thank you for registering " + localStorage.getItem("attendee_name") + "!");
    } else {
        var pathName = new URL(location.href).pathname;
        if (pathName == "/profile") {
            window.location.replace('/register');
        }
    }

    $('.register-form').on('submit', function(e) {
       e.preventDefault();
       $t = $(this);
        $.post('http://46.101.23.237:8000/profile/create/', $t.serialize(), function (response) {
            localStorage.setItem("registered", "true");
            localStorage.setItem("raw_user", JSON.stringify(response));
            for (var x in response) {
                if (response.hasOwnProperty(x)) {
                    localStorage.setItem(x, response[x]);
                }
            }
            console.log(localStorage.getItem("user"));
        }).done(function() {
            if (localStorage.getItem("registered") == "true") {
                $('.register-form').hide();
                $('.thankyou').text("Thank you for registering " + localStorage.getItem("attendee_name") + "!");
            }
        });
    });

})();