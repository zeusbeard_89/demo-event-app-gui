/**
 * Created by benja on 12/01/2017.
 */

(function() {

    if (localStorage.getItem("registered") == "true") {
        var fields = JSON.parse(localStorage.getItem("raw_user"));
        for (var k in fields){
            if (fields.hasOwnProperty(k)) {
                $("input[name="+k+"]").val(fields[k]);
            }
        }
    } else {

    }

    $('.edit_profile').on('submit', function(e) {
        e.preventDefault();
        $e = $(this);
        $.ajax({
            url: 'http://46.101.23.237:8000/profile/'+localStorage.getItem("attendee_uid")+"/update/",
            type: 'PUT',
            data: $e.serialize(),
            success: function(response) {
                localStorage.setItem("raw_user", JSON.stringify(response));
                for (var x in response) {
                    if (response.hasOwnProperty(x)) {
                        localStorage.setItem(x, response[x]);
                    }
                }
            },
            error: function(response) {
                swal("Profile Update Error", "See one of the technicians at the event", "error");
            }
        }).done(function() {
            swal("Profile Updated", "Your profile has been synced", "success");
        });
    });


})();