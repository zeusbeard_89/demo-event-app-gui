/**
 * Created by benja on 10/01/2017.
 */

(function() {

    $('.back').click(function() {
       window.history.back();
    });

    $('.menu').click(function() {
        menuslide();
    })

    $('.hide-menu').click(function() {
        menuslide();
    })

    function menuslide() {
        if(!$('.side-menu').hasClass('open')) {
            $('.side-menu').addClass('open');
            $('.side-menu').animate({
                left: "0"
            }, 100);
            $('.main-holder').animate({
                "margin-left": "200px"
            }, 100);
        } else {
            $('.side-menu').removeClass('open');
            $('.side-menu').animate({
                left: "-200"
            }, 100);
            $('.main-holder').animate({
                "margin-left": "0"
            }, 100);
        }
    }



})();