/**
 * Created by benja on 10/01/2017.
 */
var express = require('express');
var router = express.Router();
var request = require('request');

router.get('/', function (req, res, next) {
    request.get("http://46.101.23.237:8000/speakers/", function (error, response, body) {
        speakers = JSON.parse(body);
        res.render('speakers/index', {speakers: speakers});
    });
});

module.exports = router;
