/**
 * Created by benja on 10/01/2017.
 */
var express = require('express');
var router = express.Router();
var request = require('request');

router.get('/', function (req, res, next) {
    var url = "http://localhost:8000/sessions/";
    request.get(url, function (error, response, body) {
        var agenda = JSON.parse(body);
        res.render('agenda/index', {agenda: agenda});
    });
});

module.exports = router;