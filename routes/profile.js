/**
 * Created by benja on 10/01/2017.
 */

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('users/profile', {});
});

module.exports = router;