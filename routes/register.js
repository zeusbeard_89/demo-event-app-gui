/**
 * Created by ben on 10/01/2017.
 */

var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    res.render('register', {title: 'Express'});
});

router.post('/', function (req, res) {
    res.render('index', {})
})

module.exports = router;