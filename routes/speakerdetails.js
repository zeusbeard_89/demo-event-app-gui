/**
 * Created by benja on 10/01/2017.
 */
var express = require('express');
var router = express.Router({mergeParams: true});
var request = require('request');

function getSpeakerProfile(id) {
    var url = "http://46.101.23.237:8000/speaker/" + id + "/details/";
    request.get(url, function (error, response, body) {
        var profile = JSON.parse(body);
    });
}

router.get('/', function (req, res, next) {
    var profile = getSpeakerProfile(req.params.id);
    res.render('speakers/detail', {profile: profile});
});

module.exports = router;
