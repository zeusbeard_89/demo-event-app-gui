### My Event Demo GUI

#### Not Complete!

**Current Project Status**

Not complete, outstanding tasks noted below

**Outstanding**

*   Games
*   Speaker Profile
*   Book session & Add to personal Agenda
*   Client side note taking with cloud sync
*   Chat Application
*   Venue Details
*   Some links are not working - intentionally
*   Attendee Contact Book

**Project Description**

For this part of the project I chose to use ExpressJS and Jade as the templating language, I know you require Angular however with the 2 evenings I have used to prototype this, I made do with Jade and jQuery. The App itself is best viewed on a mobile device.

**Design & layout**

I chose a clean, crisp design and flat colour palette to give it a modern look. The layout I followed inspiration from your demo app of a column, also the Facebook app and Twitter or sections. The main bulk of the layout is designed using flexbox, with only a little bit of bootstrap.

**Experience**

I followed a content first approach by allowed access to all content without restriction, the only “restricted” page is the profile page which you need to register before you can access.

**Technologies**

As mentioned before as this was quick prototype I chose jQuery to add some life to the app, its quick and easily manipulated, this code also translates into Angular easily. For the template engine I chose the Express default of Jade, again a conscious time driven choice. The css was built in SASS and compiled using Gulp.

I have included a list of features if i was building this for production that i would include

**Wishlist / Feature List**

*   Full User Profile
*   Gamification, making use of realtime data (sockets, firebase, etc)
*   Social Media intergration - Auto image tagging with event, etc
*   Chat Application
*   Desktop interface as well as mobile
*   Event slides / file download
*   Venue map with booth / session location (maybe user locations)
*   Advertising space
*   Complete offline sync
*   Bundle all static files into one file for offline use
*   Update to react native app rather than container app