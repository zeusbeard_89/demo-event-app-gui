var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('styles', function() {
    gulp.src('./static-dev/scss/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/stylesheets/'))
});

//Watch task
gulp.task('default',function() {
    gulp.watch('./static-dev/scss/*.scss',['styles']);
});